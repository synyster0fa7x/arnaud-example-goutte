<?php

// chargement librairies installées via composer
require __DIR__ . '/vendor/autoload.php';

// chargement de goutte et de guzzle
$client = new Goutte\Client();
$guzzleClient = new GuzzleHttp\Client(array(
    'timeout' => 60,
));
$client->setClient($guzzleClient);

// création du crawler
$crawler = $client->request('GET', 'https://www.programme-tv.net/programme/programme-tnt.html');


// on boucle sur chaque programme de la soirée
// trim et rtrim suppriment les espaces en début et fin de chaine
$programs = $crawler->filter('.clearfix.p-v-md.bgc-white.bb-grey-3 > .programme')->each(function($node) {
    $today = new DateTime();

    $program['date_start'] = $today->format('d/m/Y').' '.$node->filter('.prog_heure')->text();
    $program['title'] = trim(rtrim($node->filter('.prog_name')->text()));
    $program['hour'] = $node->filter('.prog_heure')->text();
    $program['type'] = $node->filter('.prog_type')->text();
    $program['channel'] = str_replace('Programme', '', trim(rtrim($node->parents()->filter('.channel_label')->text())));
    $program['diff'] = $node->filter('.prog_infosDiff')->text();
    $program['duration'] = $node->filter('.date')->text();

    return $program;
});

echo '<pre>';var_dump($programs);die();
